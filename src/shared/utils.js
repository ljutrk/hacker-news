const nameHelper = (value, type) => {
    return value === 1 ? `${value} ${type}` : `${value} ${type}s`;
};

const postTime = (postDate) => {
    const currentDate = new Date().getTime();
    const dateDifference = currentDate/1000 - postDate;
    let dividedValue;

    if (dateDifference / 86400 > 1) {
        dividedValue = parseInt((dateDifference / 86400), 10);
        return `${nameHelper(dividedValue, "Day")}`;

    } else if (dateDifference / 3600 > 1) {
        dividedValue = parseInt((dateDifference / 3600), 10);
        return `${nameHelper(dividedValue, "Hour")}`;

    } else if (dateDifference / 60 > 1) {
        dividedValue = parseInt((dateDifference / 60), 10);
        return `${nameHelper(dividedValue, "Minute")}`;

    } else {
        return "few sec";
    }
};

export { postTime };