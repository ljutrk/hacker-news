import firebase from 'firebase/app';
import 'firebase/database';

const config = {
    // apiKey: "",
    // authDomain: "",
    databaseURL: "https://hacker-news.firebaseio.com"
    // projectId: "",
    // storageBucket: "",
    // messagingSenderId: "",
};

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

const db = firebase.database();

export { db };