import { db } from './firebase';
import Story from '../entities/Story';
import Comment from '../entities/Comment';

const fetchTopStories = () => {
    return new Promise((r) => {
        const dataArray = [];

        db.ref("v0/topstories").once("value", topStories => {
            topStories.val().forEach((childName) => {
                dataArray.push(db.ref(`v0/item/${childName}`).once("value").then(r => {
                    return new Story(r.val());
                }));
            });

            Promise.all(dataArray)
                .then((topStories) => {
                    r(topStories);
                });
        });
    });
};

const fetchSingleStory = (id) => {
    return new Promise((r) => {
        db.ref(`v0/item/${id}`).once("value", story => {
            r(new Story(story.val()));
        });
    });
};

const fetchSingleComment = (id) => {
    return new Promise((r) => {
        db.ref(`v0/item/${id}`).once("value", comment => {
            r(new Comment(comment.val()));
        });
    });
};

export { fetchTopStories, fetchSingleStory, fetchSingleComment };