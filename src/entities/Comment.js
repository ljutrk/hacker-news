class Comment {
    constructor(comment) {
        this.by = comment.by;
        this.id = comment.id;
        this.time = comment.time;
        this.text = comment.text;
        this.type = comment.type;
        this.parent = comment.parent;
        this.kids = comment.kids;
    }
}

export default Comment;