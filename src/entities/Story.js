class Story {
    constructor(story) {
        this.by = story.by;
        this.id = story.id;
        this.score = story.score;
        this.time = story.time;
        this.title = story.title;
        this.type = story.type;
        this.url = story.url;
        this.descendants = story.descendants;
        this.kids = story.kids;
    }
}

export default Story;