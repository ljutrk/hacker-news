import React, { Component } from 'react';
import Header from './components/partials/Header';
import { Main } from './Main';
import './App.css';

class App extends Component {
    render() {
        return (
            <div className="appDiv">
                <Header />
                <Main />
            </div>
        );
    }
}

export default App;
