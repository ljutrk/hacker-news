import React from 'react';
import { Route } from 'react-router-dom';
import { HomePage } from './components/HomePage';
import { SingleStoryPage } from './components/SingleStoryPage';

const Main = () => {
    return (
        <div className="mainDiv">
            <Route exact path="/" component={HomePage} />
            <Route path="/item/:id" component={SingleStoryPage} />
        </div>
    );
};

export { Main };