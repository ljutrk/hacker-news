import React, { Component } from 'react';
import { StoryItem } from './StoryItem';
import { database } from '../../firebase/index';
import { Loader } from './partials/Loader';

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            topStories: [],
            isLoading: true,
            page: 1
        };

        this.pageClickHandler = this.pageClickHandler.bind(this);
    }

    componentDidMount() {
        database.fetchTopStories()
            .then(topStories => {
                this.setState({
                    topStories,
                    isLoading: false
                });
            });
    }

    pageClickHandler() {
        this.setState((prevState) => ({ page: prevState.page + 1 }));
    }

    render() {
        const { topStories, page, isLoading } = this.state;
        const range = 30 * page;

        if (isLoading) {
            return <Loader />;
        }

        return (
            <div className="homePageDiv">
                {topStories
                    .slice(0, range)
                    .map((story, index) => (
                        <StoryItem 
                            story={story} 
                            key={story.id} 
                            index={index} 
                            history={this.props.history} />
                    ))
                }
                
                <button className="loadMoreStoriesButton" onClick={this.pageClickHandler}>More</button>
            </div>
        );
    }
}

export { HomePage };