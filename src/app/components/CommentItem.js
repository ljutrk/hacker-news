import React from 'react';
import { postTime } from '../../shared/utils';

const CommentItem = (props) => {
    const { comment, level, isCollapsed } = props;
    const indentationFix = { paddingLeft: 30 * level + "px" };

    return (
        <div className="commentItemDiv" style={indentationFix}>
            <div className="empty">

            </div>
            <div className="commentItemHeaderDiv">
                <ul className="commentHeaderUL">
                    <li>{comment.by}</li>
                    <li>{postTime(comment.time) + " ago"}</li>
                    <li id="collapseButton" onClick={props.collapseComments}>{`[ ${isCollapsed ? "+" : "-"} ]`} </li>
                </ul>
            </div>
            <div className="empty">

            </div>
            <div className="commentItemBodyDiv">
                {!isCollapsed ? (comment.deleted ? <p>[DELETED COMMENT]</p> : <div
                    dangerouslySetInnerHTML={{ __html: comment.text }}
                />) : null}
            </div>
        </div>
    );
};


export { CommentItem };