import React from 'react';
import urlParse from 'url-parse';
import { postTime } from '../../shared/utils';

const StoryItem = (props) => {
    const { story, index } = props;

    const clickHandler = (e) => {
        const storyURL = props.story.url;
        e.currentTarget.dataset.id === "storyComments" && props.history.push(`/item/${e.target.id}`);
        e.currentTarget.dataset.id === "storyLink" && window.open(storyURL, "_blank");
    };

    return (
        <div className="storyItemDiv" >
            <div className="storyItemindexDiv">
                <p>{typeof index === "number" && index + 1 + "."}</p>
            </div>
            <div className="storyItemHeaderDiv">
                <p data-id="storyLink" onClick={clickHandler}>{story.title}</p>
                <p><span>({urlParse(story.url).hostname})</span></p>
            </div>
            <div className="empty">

            </div>
            <div className="storyItemBodyDiv">
                <p>{story.score} points</p>
                <p>by {story.by}</p>
                <p>{postTime(story.time)} ago |</p>
                <p data-id="storyComments" onClick={clickHandler}><span id={story.id} className="storyItemBodyDivSpan">{story.descendants} comments</span></p>
            </div>
        </div>
    );
};

export { StoryItem };