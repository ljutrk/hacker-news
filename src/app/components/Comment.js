import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { database } from '../../firebase/index';
import { CommentItem } from './CommentItem';

class Comment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: null,
            kids: null,
            isCollapsed: false,
            isLoading: true
        };
        this.renderComments = this.renderComments.bind(this);
        this.collapseComments = this.collapseComments.bind(this);
    }

    componentDidMount() {
        const { comment } = this.props;
        database.fetchSingleComment(comment)
            .then(comment => {
                this.setState({
                    comment,
                    kids: comment.kids,
                    isLoading: false
                });
            });
    }

    collapseComments() {
        this.state.isCollapsed ? this.setState({ kids: this.state.comment.kids }) : this.setState({ kids: [] });
        this.setState({
            isCollapsed: !this.state.isCollapsed,
        });
    }

    renderComments(comment, kids, level, isCollapsed) {
        return (
            <div className="commentDiv">
                <CommentItem comment={comment} isCollapsed={isCollapsed} kids={kids} level={level} collapseComments={this.collapseComments}/>
                <div>
                    {(kids && kids.length > 0) && kids.map(kid => {
                        return <Comment comment={kid} key={kid} level={this.props.level + 1} />;
                    })}
                </div>
            </div>
        );
    }

    render() {
        const { comment, kids, isCollapsed, isLoading } = this.state;
        const { level } = this.props;
        
        if (isLoading) {
            return null;
        }

        return (
            <div className="commentDiv">
                {this.renderComments(comment, kids, level, isCollapsed)}
            </div>
        );
    }
}

export default withRouter(Comment);
