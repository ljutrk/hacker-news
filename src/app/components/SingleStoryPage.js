import React, { Component } from 'react';
import Comment from './Comment';
import { database } from '../../firebase/index';
import { StoryItem } from './StoryItem';
import { Loader } from './partials/Loader';

class SingleStoryPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            story: {},
            comments: [],
            isLoading:true
        };
    }

    componentDidMount() {
        const { id } = this.props.match.params;

        database.fetchSingleStory(id)
            .then(story => {
                this.setState({
                    story,
                    comments: story.kids,
                    isLoading:false
                });
            });
    }

    render() {
        const { story, comments, isLoading } = this.state;
        
        if (isLoading) {
            return <Loader />;
        }

        return (
            <div className="SingleStoryPageDiv">
                <StoryItem story={story} key={story.id} history={this.props.history}/>
                <br/>
                {comments.map(commentID => {
                    return <Comment comment={commentID} key={commentID} level={0} />;
                })}
            </div>
        );
    }
}

export { SingleStoryPage };