import React from 'react';
import logo from '../../../images/hackerNewsLogo.gif';
import { withRouter } from 'react-router-dom';

const Header = (props) => {
    const clickHandler = () => {
        props.history.push("/");
    };

    return (
        <div className="headerDiv">
            <img onClick={clickHandler} id="logo" src={logo} alt="" />
            <span onClick={clickHandler}>Hacker News</span>
        </div>
    );
};

export default withRouter(Header);