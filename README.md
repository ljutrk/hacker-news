## Hacker News

This is hacker-news clone project. You can use it to browse top stories with comments, from the official hacker news website as it uses offical hacker news API.  

Project was done using React.js with create react app.

## Installation  

* Install node dependencies 

```
$ yarn  
```

* Start dev server

```
$ yarn start  
```